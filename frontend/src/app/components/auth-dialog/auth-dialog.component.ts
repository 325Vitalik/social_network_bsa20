import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogType } from '../../models/common/auth-dialog-type';
import { Subject } from 'rxjs';
import { AuthenticationService } from '../../services/auth.service';
import { takeUntil } from 'rxjs/operators';
import { SnackBarService } from '../../services/snack-bar.service';
import { ResetPasswordService } from 'src/app/services/reset-password.service';
import { NewPassword } from 'src/app/models/password/newPassword';

@Component({
    templateUrl: './auth-dialog.component.html',
    styleUrls: ['./auth-dialog.component.sass']
})
export class AuthDialogComponent implements OnInit, OnDestroy {
    public dialogType = DialogType;
    public userName: string;
    public password: string;
    public confirmPassword: string;
    public avatar: string;
    public email: string;
    public firstTitle: string;

    public hidePass = true;
    public title: string;
    private unsubscribe$ = new Subject<void>();

    constructor(
        private dialogRef: MatDialogRef<AuthDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private authService: AuthenticationService,
        private snackBarService: SnackBarService,
        private resetPasswordService: ResetPasswordService
    ) { }

    public ngOnInit() {
        this.avatar = 'https://avatars.mds.yandex.net/get-ott/374297/2a000001616b87458162c9216ccd5144e94d/orig';
        if (this.data.dialogType === DialogType.SignIn) {
            this.title = 'Sign In';
            this.firstTitle = "Tell me who you are and";
        } else if (this.data.dialogType === DialogType.SignUp) {
            this.title = 'Sign Up';
            this.firstTitle = "Tell me who you are and";
        } else {
            this.title = 'New password';
            this.firstTitle = "Set "
        }

    }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public close() {
        this.dialogRef.close(false);
    }

    public signIn() {
        this.authService
            .login({ email: this.email, password: this.password })
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((response) => this.dialogRef.close(response), (error) => this.snackBarService.showErrorMessage(error));
    }

    public signUp() {
        this.authService
            .register({ userName: this.userName, password: this.password, email: this.email, avatar: this.avatar })
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((response) => this.dialogRef.close(response), (error) => this.snackBarService.showErrorMessage(error));
    }

    public openResetPassword() {
        this.title = this.data.dialogType = 'Reset password';
        this.data.dialogType = DialogType.ResetPassword;
        this.firstTitle = "Tell me your email ";
    }

    public sendEmail() {
        this.resetPasswordService.SendEmailWithResetPasswordLink(this.email)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((response) => { this.dialogRef.close(response); this.snackBarService.showUsualMessage("Link to reset password page was sent on your email") }, (error) => this.snackBarService.showErrorMessage(error));
    }

    public setPassword() {
        if (this.password != this.confirmPassword) {
            this.snackBarService.showErrorMessage("Password isn't confirmed");
        }

        let passwordDTO = { Password: this.password, ConfirmPassword: this.confirmPassword } as NewPassword;

        this.resetPasswordService.SendNewPassword(passwordDTO, this.data.token)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((response) => { this.dialogRef.close(response); this.snackBarService.showUsualMessage("Password changed") }, (error) => this.snackBarService.showErrorMessage(error));;
    }
}
