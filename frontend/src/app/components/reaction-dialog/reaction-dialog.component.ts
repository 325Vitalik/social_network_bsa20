import { Component, OnInit, Inject, OnDestroy, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogType } from '../../models/common/auth-dialog-type';
import { Subject } from 'rxjs';
import { User } from 'src/app/models/user';
import { FocusMonitor } from '@angular/cdk/a11y';

@Component({
    templateUrl: './reaction-dialog.component.html',
    styleUrls: ['./reaction-dialog.component.sass']
})
export class ReactionDialogComponent implements OnInit, OnDestroy {
    public dialogType = DialogType;
    public users: Array<User>;
    public title: string;
    public entityName: string;

    private unsubscribe$ = new Subject<void>();

    constructor(
        private dialogRef: MatDialogRef<ReactionDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
    ) { }

    ngOnInit(): void {
        this.users = this.data.users;
        this.title = this.data.title;
        this.entityName = this.data.entityName;
    }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public close() {
        this.dialogRef.close(false);
    }
}
