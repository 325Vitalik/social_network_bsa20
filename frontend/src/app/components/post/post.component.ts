import { Component, Input, OnDestroy, ViewChild, ContentChildren } from '@angular/core';
import { Post } from '../../models/post/post';
import { AuthenticationService } from '../../services/auth.service';
import { AuthDialogService } from '../../services/auth-dialog.service';
import { empty, Observable, Subject } from 'rxjs';
import { DialogType } from '../../models/common/auth-dialog-type';
import { ReactionService } from '../../services/reaction.service';
import { NewComment } from '../../models/comment/new-comment';
import { CommentService } from '../../services/comment.service';
import { User } from '../../models/user';
import { Comment } from '../../models/comment/comment';
import { catchError, switchMap, takeUntil } from 'rxjs/operators';
import { SnackBarService } from '../../services/snack-bar.service';
import { NewPost } from 'src/app/models/post/new-post';
import { PostService } from 'src/app/services/post.service';
import { HubConnection, HubConnectionBuilder } from '@aspnet/signalr';
import { ReactionDialogService } from 'src/app/services/reaction-dialog.service';

@Component({
    selector: 'app-post',
    templateUrl: './post.component.html',
    styleUrls: ['./post.component.sass'],
    host: {
        '(document:click)': 'onClickAnywhere($event)',
    },
})
export class PostComponent implements OnDestroy {
    @Input() public post: Post;
    @Input() public currentUser: User;

    public showComments = false;
    public showSettings = false;
    public showEdit = false;
    public postExsist = true;
    public newComment = {} as NewComment;
    public editedPost = {} as NewPost;
    public imageFile: File;
    public postLikes: number;
    public postDislikes: number;
    public usersThatLiked: Array<User>;
    public usersThatDisliked: Array<User>;
    public showShare = false;
    public link: string;

    public postHub: HubConnection;

    private unsubscribe$ = new Subject<void>();

    public constructor(
        private authService: AuthenticationService,
        private authDialogService: AuthDialogService,
        private reactionDialogService: ReactionDialogService,
        private reactionService: ReactionService,
        private commentService: CommentService,
        private snackBarService: SnackBarService,
        private postService: PostService
    ) { }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public ngOnInit() {
        this.postLikes = this.post.reactions.filter(r => r.isLike).length;
        this.postDislikes = this.post.reactions.length - this.postLikes;

        this.usersThatLiked = this.post.reactions.filter(r => r.isLike).map(r => r.user);
        this.usersThatDisliked = this.post.reactions.filter(r => !r.isLike).map(r => r.user);

        let sortedComments = this.sortCommentArray(this.post.comments);
        this.post.comments = sortedComments;

        this.registerHub();

        this.link = "http://" + window.location.host + '/post/' + this.post.id;
    }

    public toggleComments() {
        let changeShowCommentsToggle = (): void => { this.showComments = !this.showComments };
        if (!this.currentUser) {
            this.validateUser(changeShowCommentsToggle);
            return;
        }
        this.showComments = !this.showComments;
    }

    public reactPost(isLike: boolean) {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) => this.reactionService.reactPost(this.post, userResp, isLike)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe((post) => (this.post = post));

            return;
        }

        this.reactionService
            .reactPost(this.post, this.currentUser, isLike)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((post) => (this.post = post));

        this.ngOnInit();
    }

    public sendComment() {
        this.newComment.authorId = this.currentUser.id;
        this.newComment.postId = this.post.id;

        if (this.newComment.body == "" || this.newComment.body == undefined) {
            this.snackBarService.showErrorMessage("Comment must have text");
            return;
        }

        this.commentService
            .createComment(this.newComment)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    if (resp) {
                        this.post.comments = this.sortCommentArray(this.post.comments.concat(resp.body));
                        this.newComment.body = undefined;
                    }
                },
                (error) => this.snackBarService.showErrorMessage(error)
            );
    }

    public openAuthDialog() {
        this.authDialogService.openAuthDialog(DialogType.SignIn);
    }

    private validateUser(callBack: Function) {
        this.catchErrorWrapper(this.authService.getUser())
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((user) => {
                if (user) {
                    this.currentUser = user;
                    callBack()
                }
            });
    }

    private catchErrorWrapper(obs: Observable<User>) {
        return obs.pipe(
            catchError(() => {
                this.openAuthDialog();
                return empty();
            })
        );
    }

    private sortCommentArray(array: Comment[]): Comment[] {
        return array.sort((a, b) => +new Date(b.createdAt) - +new Date(a.createdAt));
    }

    public togglePostSettings() {
        this.showSettings = !this.showSettings;
    }

    public editPost() {
        let showEdit = (): void => {
            if (this.currentUser.id == this.post.author.id)
                this.showEdit = !this.showEdit
        };

        if (!this.currentUser) {
            this.validateUser(showEdit);
            return;
        }

        showEdit();
        this.togglePostSettings();
    }

    public sendEditedPost() {
        let editedPost: NewPost = {} as NewPost;
        editedPost.authorId = this.currentUser.id;
        editedPost.body = this.post.body;
        editedPost.previewImage = this.post.previewImage;

        if ((editedPost.body == null || editedPost.body == undefined || editedPost.body == "")
            && (editedPost.previewImage == null || editedPost.previewImage == undefined || editedPost.previewImage == "")) {
            this.snackBarService.showErrorMessage("The post must have image or text");
            return;
        }

        this.postService.editPost(editedPost, this.post.id)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    if (resp) {
                        this.imageFile = undefined;
                    }
                },
                (error) => this.snackBarService.showErrorMessage(error)
            );

        this.showEdit = !this.showEdit;
    }

    public loadImage(target: any) {
        this.imageFile = target.files[0];

        if (!this.imageFile) {
            target.value = '';
            return;
        }

        if (this.imageFile.size / 1000000 > 5) {
            target.value = '';
            this.snackBarService.showErrorMessage(`Image can't be heavier than ~5MB`);
            return;
        }

        const reader = new FileReader();
        reader.addEventListener('load', () => (this.post.previewImage = reader.result as string));
        reader.readAsDataURL(this.imageFile);
    }

    public removeImage() {
        this.post.previewImage = undefined;
        this.imageFile = undefined;
    }

    public deletePost() {
        if (this.currentUser != null && this.post.author.id == this.currentUser.id) {
            this.postService.deletePost(this.post.id)
                .pipe(takeUntil(this.unsubscribe$))
                .subscribe(
                    (resp) => {
                        if (resp) {
                            this.postExsist = false;
                        }
                    },
                    (error) => this.snackBarService.showErrorMessage(error)
                );;
        }
        this.togglePostSettings();
    }

    public registerHub() {
        this.postHub = new HubConnectionBuilder().withUrl('https://localhost:44344/notifications/post').build();
        this.postHub.start().catch((error) => this.snackBarService.showErrorMessage(error));

        this.postHub.on('NewComment', (newComment: Comment) => {
            if (newComment) {
                let sortedComments = this.sortCommentArray(this.post.comments);
                this.post.comments = sortedComments;
            }
        });
    }

    public onClickAnywhere(event: any) {
        if (event.target.id == "settingsButton" || event.target.id == "shareButton") {
            return;
        } else if (this.showSettings || this.showShare) {
            this.togglePostSettings();
            this.showShare = !this.showShare;
        }
    }

    public showLikes() {
        this.reactionDialogService.openReactionDialog(this.usersThatLiked, "liked", "post");
    }

    public showDislikes() {
        this.reactionDialogService.openReactionDialog(this.usersThatDisliked, "disliked", "post");
    }

    public shareByEmail() {
        let subject = "See post from Thread.NET";
        let body = "Link: " + this.link;

        let mail = "mailto:?subject=" + subject + "&body=" + body;
        window.open(mail, 'emailWindow');
    }
}
