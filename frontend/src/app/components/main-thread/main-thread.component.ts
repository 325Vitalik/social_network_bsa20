import { Component, OnInit, OnDestroy } from '@angular/core';
import { Post } from '../../models/post/post';
import { User } from '../../models/user';
import { Subject } from 'rxjs';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { AuthenticationService } from '../../services/auth.service';
import { PostService } from '../../services/post.service';
import { AuthDialogService } from '../../services/auth-dialog.service';
import { DialogType } from '../../models/common/auth-dialog-type';
import { EventService } from '../../services/event.service';
import { ImgurService } from '../../services/imgur.service';
import { NewPost } from '../../models/post/new-post';
import { switchMap, takeUntil } from 'rxjs/operators';
import { HubConnectionBuilder, HubConnection } from '@aspnet/signalr';
import { SnackBarService } from '../../services/snack-bar.service';
import { FilterDialogService } from 'src/app/services/filter-dialog.service';
import { FilterSettings } from 'src/app/models/common/filter-settings';
import { Reaction } from 'src/app/models/reactions/reaction';
import { NewReaction } from 'src/app/models/reactions/newReaction';
import { UserService } from 'src/app/services/user.service';
import { RangeSliderSettings } from 'src/app/models/common/rangeSliderSettings';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-main-thread',
    templateUrl: './main-thread.component.html',
    styleUrls: ['./main-thread.component.sass']
})
export class MainThreadComponent implements OnInit, OnDestroy {
    public posts: Post[] = [];
    public cachedPosts: Post[] = [];
    public postsBeforeSearch: Post[] = [];

    public currentUser: User;
    public imageUrl: string;
    public imageFile: File;
    public post = {} as NewPost;
    public showPostContainer = false;
    public loading = false;
    public loadingPosts = false;
    public showSearch = false;
    public isSinglePostMode = false;
    public searcString: string = '';

    public postHub: HubConnection;

    private unsubscribe$ = new Subject<void>();
    private filterSettings = {} as FilterSettings;
    public sortType: (posts: Post[], isReverse: boolean) => Post[] = this.sortByPopularity;
    public isSortReverse: boolean = false;

    public constructor(
        private snackBarService: SnackBarService,
        private authService: AuthenticationService,
        private postService: PostService,
        private imgurService: ImgurService,
        private authDialogService: AuthDialogService,
        private eventService: EventService,
        private filterDialogService: FilterDialogService,
        private userService: UserService,
        private router: Router,
        private route: ActivatedRoute
    ) { }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
        this.postHub.stop();
    }

    public ngOnInit() {
        this.registerHub();
        this.getUser();

        if (this.router.url.includes("/post")) {
            let postId = +this.route.snapshot.paramMap.get('id');
            this.postService.getPostById(postId)
                .pipe(takeUntil(this.unsubscribe$))
                .subscribe(
                    (resp) => {
                        this.loadingPosts = false;
                        this.posts = this.cachedPosts = this.postsBeforeSearch = [resp.body];
                    },
                    (error) => (this.loadingPosts = false)
                );;
            this.isSinglePostMode = true;
        }
        else if (this.router.url.includes("/reset/password")) {
            let token = this.route.snapshot.paramMap.get('token');
            this.authDialogService.openResetPasswordDialog(DialogType.NewPassword, token)
                .afterClosed()
                .pipe(takeUntil(this.unsubscribe$))
                .subscribe((result) => {
                    this.getPosts();
                    this.isSinglePostMode = false;
                });
        } else {
            this.getPosts();
            this.isSinglePostMode = false;
        }

        this.eventService.userChangedEvent$.pipe(takeUntil(this.unsubscribe$)).subscribe((user) => {
            this.currentUser = user;
            this.post.authorId = this.currentUser ? this.currentUser.id : undefined;
        });
    }

    public getPosts() {
        this.loadingPosts = true;
        this.postService
            .getPosts()
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    this.loadingPosts = false;
                    this.posts = this.cachedPosts = this.postsBeforeSearch = resp.body.map((p) => {
                        let dislikes = p.reactions.filter(r => !r.isLike).length;
                        p.popularity = (p.reactions.length - dislikes) - dislikes;
                        p.popularity += 2 * p.comments.length;
                        p.createdAt = new Date(p.createdAt);
                        return p;
                    });
                    this.constructFilter();

                    this.posts = this.sortPostArray(this.posts);
                },
                (error) => (this.loadingPosts = false)
            );
    }

    private constructFilter() {
        this.filterSettings.countOfComments = {
            type: "Range",
            min: 0,
            max: this.posts.reduce((p1, p2) => p2.comments.length > p1.comments.length ? p2 : p1).comments.length,
            range: [0, 1],
            tooltip: { placement: 'Before', isVisible: true, showOn: 'Focus' }
        } as RangeSliderSettings;

        this.filterSettings.countOfComments.range = [this.filterSettings.countOfComments.min, this.filterSettings.countOfComments.max];

        this.filterSettings.popularity = {
            type: "Range",
            min: this.posts.reduce((p1, p2) => p1.popularity < p2.popularity ? p1 : p2).popularity,
            max: this.posts.reduce((p1, p2) => p1.popularity > p2.popularity ? p1 : p2).popularity,
            range: [0, 1],
            tooltip: { placement: 'Before', isVisible: true, showOn: 'Focus' }
        } as RangeSliderSettings;

        this.filterSettings.popularity.range = [this.filterSettings.popularity.min, this.filterSettings.popularity.max];

        this.filterSettings.createdDateTo = new Date();
        this.filterSettings.createdDateFrom = this.posts[this.posts.length - 1].createdAt;
    }

    public sendPost() {
        if ((this.post.body == "" || this.post.body == undefined)
            && (this.imageUrl == "" || this.imageUrl == undefined)) {
            this.snackBarService.showErrorMessage("The post must have image or text");
            return;
        }

        const postSubscription = !this.imageFile
            ? this.postService.createPost(this.post)
            : this.imgurService.uploadToImgur(this.imageFile, 'title').pipe(
                switchMap((imageData) => {
                    this.post.previewImage = imageData.body.data.link;
                    return this.postService.createPost(this.post);
                })
            );

        this.loading = true;

        postSubscription.pipe(takeUntil(this.unsubscribe$)).subscribe(
            (respPost) => {
                this.addNewPost(respPost.body);
                this.removeImage();
                this.post.body = undefined;
                this.post.previewImage = undefined;
                this.loading = false;
                this.showPostContainer = false;
            },
            (error) => this.snackBarService.showErrorMessage(error)
        );
    }

    public loadImage(target: any) {
        this.imageFile = target.files[0];

        if (!this.imageFile) {
            target.value = '';
            return;
        }

        if (this.imageFile.size / 1000000 > 5) {
            target.value = '';
            this.snackBarService.showErrorMessage(`Image can't be heavier than ~5MB`);
            return;
        }

        const reader = new FileReader();
        reader.addEventListener('load', () => (this.imageUrl = reader.result as string));
        reader.readAsDataURL(this.imageFile);
    }

    public removeImage() {
        this.imageUrl = undefined;
        this.imageFile = undefined;
    }

    public toggleNewPostContainer() {
        this.showPostContainer = !this.showPostContainer;
    }

    public openAuthDialog() {
        this.authDialogService.openAuthDialog(DialogType.SignIn);
    }

    public registerHub() {
        this.postHub = new HubConnectionBuilder().withUrl('https://localhost:44344/notifications/post').build();
        this.postHub.start().catch((error) => this.snackBarService.showErrorMessage(error));

        this.postHub.on('NewPost', (newPost: Post) => {
            if (newPost) {
                this.addNewPost(newPost);
            }
        });


        this.postHub.on('PostReaction', (reaction: NewReaction, postReaction: Reaction) => {
            if (reaction) {
                if (this.currentUser != null &&
                    this.cachedPosts.find(p => p.id === reaction.entityId).author.id == this.currentUser.id) {
                    this.userService.getUserById(reaction.userId)
                        .pipe(takeUntil(this.unsubscribe$))
                        .subscribe((user) => {
                            let userThatLikedPost = user.body;

                            if (reaction.isLike) {
                                this.snackBarService.showNotitfication(`${userThatLikedPost.userName} liked your post`);
                            } else {
                                this.snackBarService.showNotitfication(`${userThatLikedPost.userName} disliked your post`);
                            }
                        });
                }
            }
        });
    }

    public addNewPost(newPost: Post) {
        if (!this.cachedPosts.some((x) => x.id === newPost.id)) {
            this.cachedPosts = this.cachedPosts.concat(newPost);
            this.posts = this.sortPostArray(this.posts.concat(newPost));
        }
        this.showPostContainer = !this.showPostContainer;
    }

    private getUser() {
        this.authService
            .getUser()
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((user) => (this.currentUser = user));
    }

    private sortPostArray(array: Post[]): Post[] {
        return this.sortType(array, this.isSortReverse);
    }

    public sortByPopularity(posts: Post[], isReverse: boolean): Post[] {
        let arrayofPosts = posts.sort((a, b) => b.popularity - a.popularity);
        if (!isReverse) {
            return arrayofPosts;
        } else {
            return arrayofPosts.reverse();
        }
    }

    public sortByDate(posts: Post[], isReverse: boolean): Post[] {
        let arrayofPosts = posts.sort((a, b) => +new Date(b.createdAt) - +new Date(a.createdAt));
        if (!isReverse) {
            return arrayofPosts;
        } else {
            return arrayofPosts.reverse();
        }
    }

    public sortByCountOfComments(posts: Post[], isReverse: boolean): Post[] {
        let arrayofPosts = posts.sort((a, b) => b.comments.length - a.comments.length);
        if (!isReverse) {
            return arrayofPosts;
        } else {
            return arrayofPosts.reverse();
        }
    }

    public setSortType(sortType: (posts: Post[], isReverse: boolean) => Post[] = this.sortType) {
        this.sortType = sortType;
        this.posts = this.sortPostArray(this.posts);
    }

    public openFilter() {
        this.filterDialogService.openFilterDialog(this.cachedPosts, this.currentUser, this.filterSettings)
            .afterClosed().subscribe(result => {
                this.filterSettings = result.settings;
                this.posts = this.postsBeforeSearch = this.sortPostArray(result.posts);
            });
    }

    public search() {
        this.posts = this.postsBeforeSearch.filter(p => p.body.toLocaleLowerCase().includes(this.searcString.toLocaleLowerCase()));
    }
}
