import { Component, OnInit, Inject, OnDestroy, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogType } from '../../models/common/auth-dialog-type';
import { Subject } from 'rxjs';
import { User } from 'src/app/models/user';
import { Post } from 'src/app/models/post/post';
import { FilterSettings } from 'src/app/models/common/filter-settings';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS, } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { SnackBarService } from 'src/app/services/snack-bar.service';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { SliderChangeEventArgs, Slider } from "@syncfusion/ej2-inputs";
import { ColorRangeDataModel } from '@syncfusion/ej2-angular-inputs';

@Component({
    templateUrl: './filter-dialog.component.html',
    styleUrls: ['./filter-dialog.component.sass'],
    providers: [
        { provide: MAT_DATE_LOCALE, useValue: 'id-ID' },
        {
            provide: DateAdapter,
            useClass: MomentDateAdapter,
            deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
        },
        { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS },
    ],
})
export class FilterDialogComponent implements OnInit, OnDestroy {
    public dialogType = DialogType;
    public filterSettings: FilterSettings;
    public color: ColorRangeDataModel[] = [{ color: "green", start: 0, end: 100 } as ColorRangeDataModel];
    public slider = new Slider()

    private unsubscribe$ = new Subject<void>();
    private allPosts: Array<Post>;
    private filteredPosts: Array<Post>;
    private currentUser: User;

    constructor(
        private dialogRef: MatDialogRef<FilterDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private snackBarService: SnackBarService
    ) { }

    ngOnInit(): void {
        this.filteredPosts = this.allPosts;
        this.currentUser = this.data.currentUser;
        this.filterSettings = this.data.settings;
        this.allPosts = this.data.posts;

        this.filterSettings.countOfComments.max = this.allPosts.sort((p1, p2) => p2.comments.length - p1.comments.length)[0].comments.length;
        this.filterSettings.popularity.max = this.allPosts.sort((p1, p2) => p2.popularity - p1.popularity)[0].popularity;
    }

    private dateValidation(): boolean {
        if (!this.filterSettings.createdDateFrom || !this.filterSettings.createdDateTo
            || this.filterSettings.createdDateFrom > this.filterSettings.createdDateTo) {
            this.snackBarService.showErrorMessage("Date incorrect");
            return false;
        }
        return true;
    }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    private filterPosts() {
        this.filteredPosts = this.allPosts.filter((p) => {
            let isValid: boolean = true;
            if (this.filterSettings.isOnlyMine) {
                isValid = p.author.id === this.currentUser.id && isValid;
            } else if (this.filterSettings.isNotMine) {
                isValid = p.author.id !== this.currentUser.id && isValid;
            }

            if (this.filterSettings.isILiked) {
                isValid = p.reactions.some(r => r.user.id == this.currentUser.id && r.isLike) && isValid;
            }

            isValid = p.comments.length >= this.filterSettings.countOfComments.range[0]
                && p.comments.length <= this.filterSettings.countOfComments.range[1]
                && p.popularity >= this.filterSettings.popularity.range[0]
                && p.popularity <= this.filterSettings.popularity.range[1]
                && isValid;

            isValid = p.createdAt >= this.filterSettings.createdDateFrom && isValid;
            isValid = p.createdAt <= this.filterSettings.createdDateTo && isValid;

            return isValid;
        });
    }

    public close() {
        let result = this.dateValidation();
        if (!result) {
            return;
        }

        this.filterPosts();
        this.dialogRef.close({ posts: this.filteredPosts, settings: this.filterSettings });
    }

    public setFromDate(event: MatDatepickerInputEvent<Date>) {
        this.filterSettings.createdDateFrom = event.value;
    }

    public setToDate(event: MatDatepickerInputEvent<Date>) {
        this.filterSettings.createdDateTo = event.value;
    }

    public setCountOfComments(event: SliderChangeEventArgs) {
        this.filterSettings.countOfComments.range = event.value as number[];
    }

    public setPopularity(event: SliderChangeEventArgs) {
        this.filterSettings.popularity.range = event.value as number[];
    }

    public clear() {
        this.filterSettings.isILiked = false;
        this.filterSettings.isNotMine = false;
        this.filterSettings.isOnlyMine = false;
        this.filterSettings.createdDateFrom = this.allPosts.sort((a, b) => +new Date(a.createdAt) - +new Date(b.createdAt))[0].createdAt;
        this.filterSettings.createdDateTo = new Date();

        this.filterSettings.countOfComments.range = [this.filterSettings.countOfComments.min, this.filterSettings.countOfComments.max];
        this.filterSettings.popularity.range = [this.filterSettings.popularity.min, this.filterSettings.popularity.max];
    }
}
