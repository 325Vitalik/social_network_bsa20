import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Comment } from '../../models/comment/comment';
import { Subject, Observable, empty } from 'rxjs';
import { AuthenticationService } from 'src/app/services/auth.service';
import { ReactionService } from 'src/app/services/reaction.service';
import { User } from 'src/app/models/user';
import { catchError, takeUntil, switchMap } from 'rxjs/operators';
import { DialogType } from 'src/app/models/common/auth-dialog-type';
import { AuthDialogService } from 'src/app/services/auth-dialog.service';
import { NewComment } from 'src/app/models/comment/new-comment';
import { SnackBarService } from 'src/app/services/snack-bar.service';
import { CommentService } from 'src/app/services/comment.service';
import { ReactionDialogService } from 'src/app/services/reaction-dialog.service';

@Component({
    selector: 'app-comment',
    templateUrl: './comment.component.html',
    styleUrls: ['./comment.component.sass'],
    host: {
        '(document:click)': 'onClickAnywhere($event)',
    }
})
export class CommentComponent implements OnInit, OnDestroy {
    @Input() public comment: Comment;
    @Input() public currentUser: User;

    public commentLikes: number;
    public commentDislikes: number;
    public showSettings = false;
    public showEdit = false;
    public commentExsist = true;
    public usersThatLiked: Array<User>;
    public usersThatDisliked: Array<User>;

    private unsubscribe$ = new Subject<void>();

    public constructor(
        private authService: AuthenticationService,
        private authDialogService: AuthDialogService,
        private reactionService: ReactionService,
        private commentService: CommentService,
        private snackBarService: SnackBarService,
        private reactionDialogService: ReactionDialogService
    ) { }
    ngOnDestroy(): void {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public ngOnInit() {
        this.commentLikes = this.comment.reactions.filter(c => c.isLike).length;
        this.commentDislikes = this.comment.reactions.length - this.commentLikes;

        this.usersThatLiked = this.comment.reactions.filter(r => r.isLike).map(r => r.user);
        this.usersThatDisliked = this.comment.reactions.filter(r => !r.isLike).map(r => r.user);
    }

    public reactComment(isLike: boolean) {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) => this.reactionService.reactComment(this.comment, userResp, isLike)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe((comment) => (this.comment = comment));

            return;
        }

        this.reactionService
            .reactComment(this.comment, this.currentUser, isLike)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((comment) => (this.comment = comment));

        this.ngOnInit();
    }

    public openAuthDialog() {
        this.authDialogService.openAuthDialog(DialogType.SignIn);
    }

    private catchErrorWrapper(obs: Observable<User>) {
        return obs.pipe(
            catchError(() => {
                this.openAuthDialog();
                return empty();
            })
        );
    }

    public toggleCommentSettings() {
        this.showSettings = !this.showSettings;
    }

    public edit() {
        let showEdit = (): void => {
            if (this.currentUser.id == this.comment.author.id)
                this.showEdit = !this.showEdit
        };

        if (!this.currentUser) {
            this.validateUser(showEdit);
            return;
        }

        showEdit();
    }

    public delete() {
        if (this.currentUser != null && this.comment.author.id == this.currentUser.id) {
            this.commentService.deleteComment(this.comment.id)
                .pipe(takeUntil(this.unsubscribe$))
                .subscribe(
                    (resp) => {
                        if (resp) {
                            this.commentExsist = false;
                        }
                    },
                    (error) => this.snackBarService.showErrorMessage(error)
                );;
        }
    }

    private validateUser(callBack: Function) {
        this.catchErrorWrapper(this.authService.getUser())
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((user) => {
                if (user) {
                    this.currentUser = user;
                    callBack()
                }
            });
    }

    public sendComment() {
        let editedComment: NewComment = {
            authorId: this.currentUser.id,
            postId: 0,
            body: this.comment.body
        }

        if (editedComment.body == "") {
            this.snackBarService.showErrorMessage("Comment must have text");
            return;
        }

        this.commentService.editComment(editedComment, this.comment.id)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => { },
                (error) => this.snackBarService.showErrorMessage(error)
            );

        this.showEdit = !this.showEdit;
    }

    public showLikes() {
        this.reactionDialogService.openReactionDialog(this.usersThatLiked, "liked", "comment");
    }

    public showDislikes() {
        this.reactionDialogService.openReactionDialog(this.usersThatDisliked, "disliked", "comment");
    }

    public onClickAnywhere(event: any) {
        if (event.target.id == "settingsButton") return;
        if (this.showSettings) {
            this.toggleCommentSettings();
        }
    }
}
