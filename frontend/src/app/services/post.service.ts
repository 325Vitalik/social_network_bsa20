import { Injectable } from '@angular/core';
import { HttpInternalService } from './http-internal.service';
import { Post } from '../models/post/post';
import { NewReaction } from '../models/reactions/newReaction';
import { NewPost } from '../models/post/new-post';

@Injectable({ providedIn: 'root' })
export class PostService {
    public routePrefix = '/api/posts';

    constructor(private httpService: HttpInternalService) { }

    public getPosts() {
        return this.httpService.getFullRequest<Post[]>(`${this.routePrefix}`);
    }

    public createPost(post: NewPost) {
        return this.httpService.postFullRequest<Post>(`${this.routePrefix}`, post);
    }

    public reactPost(reaction: NewReaction) {
        return this.httpService.postFullRequest<Post>(`${this.routePrefix}/reaction`, reaction);
    }

    public editPost(post: NewPost, postId: number) {
        return this.httpService.putFullRequest<Post>(`${this.routePrefix}/${postId}`, post);
    }

    public deletePost(postId: number) {
        return this.httpService.deleteFullRequest<Post>(`${this.routePrefix}/${postId}`);
    }

    public getPostById(postId: number) {
        return this.httpService.getFullRequest<Post>(`${this.routePrefix}/${postId}`);
    }
}
