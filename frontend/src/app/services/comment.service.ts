import { Injectable } from '@angular/core';
import { HttpInternalService } from './http-internal.service';
import { NewComment } from '../models/comment/new-comment';
import { Comment } from '../models/comment/comment';
import { NewReaction } from '../models/reactions/newReaction';

@Injectable({ providedIn: 'root' })
export class CommentService {
    public routePrefix = '/api/comments';

    constructor(private httpService: HttpInternalService) { }

    public createComment(comment: NewComment) {
        return this.httpService.postFullRequest<Comment>(`${this.routePrefix}`, comment);
    }

    public reactComment(reaction: NewReaction) {
        return this.httpService.postFullRequest<Comment>(`${this.routePrefix}/reaction`, reaction);
    }

    public editComment(comment: NewComment, commentId: number) {
        return this.httpService.putFullRequest<Comment>(`${this.routePrefix}/${commentId}`, comment);
    }

    public deleteComment(commentId: number) {
        return this.httpService.deleteFullRequest<Comment>(`${this.routePrefix}/${commentId}`);
    }
}
