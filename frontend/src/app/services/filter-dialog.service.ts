import { Injectable, OnDestroy } from '@angular/core';
import { DialogType } from '../models/common/auth-dialog-type';
import { AuthDialogComponent } from '../components/auth-dialog/auth-dialog.component';
import { User } from '../models/user';
import { MatDialog } from '@angular/material/dialog';
import { map, takeUntil } from 'rxjs/operators';
import { AuthenticationService } from './auth.service';
import { Subscription, Subject } from 'rxjs';
import { FilterDialogComponent } from '../components/filter-dialog/filter-dialog.component';
import { Post } from '../models/post/post';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { FilterSettings } from '../models/common/filter-settings';

@Injectable({ providedIn: 'root' })
export class FilterDialogService implements OnDestroy {
    private unsubscribe$ = new Subject<void>();

    public constructor(private dialog: MatDialog) { }

    public openFilterDialog(posts: Array<Post>, user: User, settings: FilterSettings) {
        const dialog = this.dialog.open(FilterDialogComponent, {
            data: { posts: posts, currentUser: user, settings: settings },
            minWidth: 300,
            autoFocus: true,
            backdropClass: 'dialog-backdrop',
            disableClose: true,
            position: {
                top: '0'
            }
        });

        return dialog;
    }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }
}
