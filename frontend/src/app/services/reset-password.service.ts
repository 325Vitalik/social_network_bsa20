import { Injectable } from '@angular/core';
import { HttpInternalService } from './http-internal.service';
import { NewPassword } from '../models/password/newPassword';

@Injectable({ providedIn: 'root' })
export class ResetPasswordService {
    public routePrefix = '/api/password';

    constructor(private httpService: HttpInternalService) { }

    public SendEmailWithResetPasswordLink(email: string) {
        return this.httpService.postFullRequest(`${this.routePrefix}/send-email`, { email: email });
    }

    public SendNewPassword(password: NewPassword, token: string) {
        localStorage.setItem('accessToken', JSON.stringify(token));
        return this.httpService.postFullRequest(`${this.routePrefix}/reset`, password);
    }

}