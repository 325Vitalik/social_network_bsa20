import { Injectable, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { User } from '../models/user';
import { ReactionDialogComponent } from '../components/reaction-dialog/reaction-dialog.component';


@Injectable({ providedIn: 'root' })
export class ReactionDialogService implements OnDestroy {
    private unsubscribe$ = new Subject<void>();

    public constructor(private dialog: MatDialog) { }

    public openReactionDialog(users: Array<User>, title: string, entityName: string) {
        const dialog = this.dialog.open(ReactionDialogComponent, {
            data: { users: users, title: title, entityName: entityName },
            minWidth: 300,
            autoFocus: true,
            backdropClass: 'dialog-backdrop',
            position: {
                top: '0'
            }
        });
    }

    ngOnDestroy(): void {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }
}