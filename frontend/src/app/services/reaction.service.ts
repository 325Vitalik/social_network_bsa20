import { Injectable } from '@angular/core';
import { AuthenticationService } from './auth.service';
import { Post } from '../models/post/post';
import { Comment } from '../models/comment/comment';
import { NewReaction } from '../models/reactions/newReaction';
import { PostService } from './post.service';
import { User } from '../models/user';
import { map, catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { Reaction } from '../models/reactions/reaction';
import { CommentService } from './comment.service';

@Injectable({ providedIn: 'root' })
export class ReactionService {
    public constructor(private authService: AuthenticationService,
        private postService: PostService,
        private commentSrvice: CommentService) { }

    public reactPost(post: Post, currentUser: User, isLike: boolean) {
        const innerPost = post;
        const arrayToRevert = innerPost.reactions;

        const reaction: NewReaction = {
            entityId: innerPost.id,
            isLike: isLike,
            userId: currentUser.id
        };

        // update current array instantly
        let hasReaction = innerPost.reactions.some((x) => x.user.id === currentUser.id);
        if (hasReaction) {
            if (innerPost.reactions.some((x) => x.user.id === currentUser.id
                && x.isLike === reaction.isLike)) {
                innerPost.reactions = innerPost.reactions.filter((x) => x.user.id !== currentUser.id);
            }
            else {
                let index = innerPost.reactions.findIndex((x) => x.user.id === currentUser.id);
                innerPost.reactions[index].isLike = reaction.isLike;
            }
        }
        else {
            innerPost.reactions.push({ isLike: reaction.isLike, user: currentUser } as Reaction);
        }

        return this.postService.reactPost(reaction).pipe(
            map(() => innerPost),
            catchError(() => {
                innerPost.reactions = arrayToRevert;

                return of(innerPost);
            })
        );
    }

    public reactComment(comment: Comment, currentUser: User, isLike: boolean) {
        const innerComment = comment;
        const arrayToRevert = comment.reactions;

        const reaction: NewReaction = {
            entityId: innerComment.id,
            isLike: isLike,
            userId: currentUser.id
        };

        let hasReaction = innerComment.reactions.some((x) => x.user.id === currentUser.id);
        if (hasReaction) {
            if (innerComment.reactions.some((x) => x.user.id === currentUser.id
                && x.isLike === reaction.isLike)) {
                innerComment.reactions = innerComment.reactions.filter((x) => x.user.id !== currentUser.id);
            }
            else {
                let index = innerComment.reactions.findIndex((x) => x.user.id === currentUser.id);
                innerComment.reactions[index].isLike = reaction.isLike;
            }
        }
        else {
            innerComment.reactions.push({ isLike: reaction.isLike, user: currentUser } as Reaction);
        }

        return this.commentSrvice.reactComment(reaction).pipe(
            map(() => innerComment),
            catchError(() => {
                innerComment.reactions = arrayToRevert;

                return of(innerComment);
            })
        );
    }
}
