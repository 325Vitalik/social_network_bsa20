export interface RangeSliderSettings {
    type: string;
    min: number;
    max: number;
    range: number[];
    tooltip: Object;
}