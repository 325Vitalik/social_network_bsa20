import { RangeSliderSettings } from './rangeSliderSettings';

export interface FilterSettings {
    isOnlyMine: boolean;
    isILiked: boolean;
    isNotMine: boolean;
    countOfComments: RangeSliderSettings;
    popularity: RangeSliderSettings;
    createdDateFrom: Date;
    createdDateTo: Date;
}