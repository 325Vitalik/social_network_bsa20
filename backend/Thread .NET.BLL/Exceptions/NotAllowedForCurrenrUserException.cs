﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thread_.NET.BLL.Exceptions
{
    public sealed class NotAllowedForCurrenrUserException : Exception
    {
        public NotAllowedForCurrenrUserException(int id)
            : base($"Access for user ({id}) forbidden")
        { }
    }
}
