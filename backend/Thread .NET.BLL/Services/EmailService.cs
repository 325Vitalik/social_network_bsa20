﻿using System;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using Thread_.NET.Common.Notifications;

namespace Thread_.NET.BLL.Services
{
    public class EmailService
    {
        private EmailOptions settings;

        public EmailService() { }

        public void SetSettings(EmailOptions settings)
        {
            this.settings = settings;
        }

        public async Task SendNotificationAsync(string message)
        {
            if (settings == null)
            {
                throw new NullReferenceException("Settings not set!");
            }

            using var smtpClient = new SmtpClient();

            smtpClient.EnableSsl = settings.UseSsl;
            smtpClient.Host = settings.ServerName;
            smtpClient.Port = settings.ServerPort;
            smtpClient.UseDefaultCredentials = false;
            smtpClient.Credentials = new NetworkCredential(settings.Username, settings.Password);

            var body = message;

            MailMessage mailMessage = new MailMessage(
                    settings.MailFromAddress,
                    settings.MailToAddress,
                    "Thread.NET",
                    body
                    );


            await smtpClient.SendMailAsync(mailMessage);
        }
    }
}
