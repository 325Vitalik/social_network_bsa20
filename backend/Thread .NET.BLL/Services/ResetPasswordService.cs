﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Linq;
using System.Threading.Tasks;
using Thread_.NET.BLL.Exceptions;
using Thread_.NET.BLL.JWT;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.Auth;
using Thread_.NET.Common.Notifications;
using Thread_.NET.Common.Security;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;

namespace Thread_.NET.BLL.Services
{
    public class ResetPasswordService : BaseService
    {
        private readonly JwtFactory _jwtFactory;
        private readonly FrontendOptions _frontendOptions;
        private readonly EmailService _emailService;
        private readonly EmailOptions _emailOptions;

        public ResetPasswordService(ThreadContext context, IMapper mapper, JwtFactory jwtFactory, IOptions<FrontendOptions> frontendOptions, EmailService emailService, IOptions<EmailOptions> emailOptions) : base(context, mapper)
        {
            _jwtFactory = jwtFactory;
            _frontendOptions = frontendOptions.Value;
            _emailService = emailService;
            _emailOptions = emailOptions.Value;
        }

        public async Task SendResetPasswordToken(string email)
        {
            var user = _context.Users.FirstOrDefault(u => u.Email == email);

            if (user == null)
            {
                throw new NotFoundException(nameof(User));
            }

            var token = await _jwtFactory.GeneratePasswordRecoveryToken(user.Id, user.UserName, email);

            var link = _frontendOptions.Link + "reset/password/" + token.Token;

            _emailOptions.MailToAddress = email;
            _emailService.SetSettings(_emailOptions);
            await _emailService.SendNotificationAsync("Link to reset passwword: " + link);
        }

        public async Task SetNewPassword(string password, int userId)
        {
            var user = await _context.Users.FirstOrDefaultAsync(u => u.Id == userId);

            if (user == null)
            {
                throw new NotFoundException(nameof(User), userId);
            }

            var salt = SecurityHelper.GetRandomBytes();

            user.Salt = Convert.ToBase64String(salt);
            user.Password = SecurityHelper.HashPassword(password, salt);

            _context.Users.UpdateRange(user);
            await _context.SaveChangesAsync();
        }
    }
}
