﻿using AutoMapper;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;
using Thread_.NET.BLL.Exceptions;
using Thread_.NET.BLL.Hubs;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Comment;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;

namespace Thread_.NET.BLL.Services
{
    public sealed class CommentService : BaseService
    {
        private IHubContext<PostHub> _postHub;

        public CommentService(ThreadContext context, IMapper mapper, IHubContext<PostHub> postHub) : base(context, mapper)
        {
            this._postHub = postHub;
        }

        public async Task<CommentDTO> CreateComment(NewCommentDTO newComment)
        {
            var commentEntity = _mapper.Map<Comment>(newComment);

            _context.Comments.Add(commentEntity);
            await _context.SaveChangesAsync();

            var createdComment = await _context.Comments
                .Include(comment => comment.Author)
                    .ThenInclude(user => user.Avatar)
                .FirstAsync(comment => comment.Id == commentEntity.Id);

            var newCommentDTO = _mapper.Map<CommentDTO>(createdComment);

            await _postHub.Clients.All.SendAsync("NewComment", newCommentDTO);

            return newCommentDTO;
        }

        public async Task<CommentDTO> UpdateComment(NewCommentDTO updatedComment, int commentId)
        {
            var comment = await _context.Comments.FirstOrDefaultAsync(c => c.Id == commentId);

            if (comment == null)
            {
                throw new NotFoundException(nameof(Comment), commentId);
            }
            if (comment.AuthorId != updatedComment.AuthorId)
            {
                throw new NotAllowedForCurrenrUserException(updatedComment.AuthorId);
            }
            if (updatedComment.Body == null || updatedComment.Body == "")
            {
                throw new ArgumentException("data is incorrect");
            }

            comment.Body = updatedComment.Body;
            comment.UpdatedAt = DateTime.Now;

            _context.Comments.UpdateRange(comment);
            await _context.SaveChangesAsync();

            var upComment = await _context.Comments.FirstOrDefaultAsync(c => c.Id == commentId);

            await _context.Entry(upComment).Reference(c => c.Author).LoadAsync();
            await _context.Entry(upComment).Collection(c => c.Reactions).LoadAsync();

            var updatedCommentDTO = _mapper.Map<CommentDTO>(upComment);

            await _postHub.Clients.All.SendAsync("UpdateComment", updatedCommentDTO);

            return updatedCommentDTO;
        }

        public async Task DeleteComment(int commentId, int userId)
        {
            if (commentId < 0)
            {
                throw new ArgumentException($"Id should be positive");
            }

            var comment = await _context.Comments.FirstOrDefaultAsync(p => p.Id == commentId);

            if (comment == null)
            {
                throw new NotFoundException(nameof(Post), commentId);
            }
            if (comment.AuthorId != userId)
            {
                throw new NotAllowedForCurrenrUserException(commentId);
            }

            await _context.Entry(comment).Collection(c => c.Reactions).LoadAsync();

            _context.Comments.Remove(comment);
            await _context.SaveChangesAsync();

            await _postHub.Clients.All.SendAsync("DeleteComment");
        }
    }
}
