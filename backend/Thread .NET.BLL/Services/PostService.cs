﻿using AutoMapper;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thread_.NET.BLL.Exceptions;
using Thread_.NET.BLL.Hubs;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Post;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;

namespace Thread_.NET.BLL.Services
{
    public sealed class PostService : BaseService
    {
        private readonly IHubContext<PostHub> _postHub;

        public PostService(ThreadContext context, IMapper mapper, IHubContext<PostHub> postHub) : base(context, mapper)
        {
            _postHub = postHub;
        }

        public async Task<ICollection<PostDTO>> GetAllPosts()
        {
            var posts = await _context.Posts
                .Include(post => post.Author)
                    .ThenInclude(author => author.Avatar)
                .Include(post => post.Preview)
                .Include(post => post.Reactions)
                    .ThenInclude(reaction => reaction.User)
                .Include(post => post.Comments)
                    .ThenInclude(comment => comment.Reactions)
                .Include(post => post.Comments)
                    .ThenInclude(comment => comment.Author)
                .OrderByDescending(post => post.CreatedAt)
                .ToListAsync();

            return _mapper.Map<ICollection<PostDTO>>(posts);
        }

        public async Task<ICollection<PostDTO>> GetAllPosts(int userId)
        {
            var posts = await _context.Posts
                .Include(post => post.Author)
                    .ThenInclude(author => author.Avatar)
                .Include(post => post.Preview)
                .Include(post => post.Comments)
                    .ThenInclude(comment => comment.Author)
                .Where(p => p.AuthorId == userId) // Filter here
                .ToListAsync();

            return _mapper.Map<ICollection<PostDTO>>(posts);
        }

        public async Task<PostDTO> CreatePost(PostCreateDTO postDto)
        {
            var postEntity = _mapper.Map<Post>(postDto);

            _context.Posts.Add(postEntity);
            await _context.SaveChangesAsync();

            var createdPost = await _context.Posts
                .Include(post => post.Author)
                    .ThenInclude(author => author.Avatar)
                .FirstAsync(post => post.Id == postEntity.Id);

            var createdPostDTO = _mapper.Map<PostDTO>(createdPost);
            await _postHub.Clients.All.SendAsync("NewPost", createdPostDTO);

            return createdPostDTO;
        }

        public async Task<PostDTO> UpdatePost(PostCreateDTO postDto, int postId)
        {
            var post = await _context.Posts.FirstOrDefaultAsync(p => p.Id == postId);

            if (post == null)
            {
                throw new NotFoundException(nameof(Post), postId);
            }
            if (post.AuthorId != postDto.AuthorId)
            {
                throw new NotAllowedForCurrenrUserException(postDto.AuthorId);
            }
            if (postDto.Body == null && postDto.PreviewImage == null)
            {
                throw new ArgumentException("data is incorrect");
            }

            post.Preview = postDto.PreviewImage == null ? null : new Image { URL = postDto.PreviewImage };
            if (post.Preview == null) post.PreviewId = null;
            post.Body = postDto.Body;
            post.UpdatedAt = DateTime.Now;

            _context.Posts.Update(post);
            await _context.SaveChangesAsync();

            // ------------------------------------------------
            var updatedPost = await _context.Posts
                    .Include(post => post.Author)
                        .ThenInclude(author => author.Avatar)
                    .FirstAsync(p => p.Id == post.Id);

            var createdPostDTO = _mapper.Map<PostDTO>(updatedPost);
            await _postHub.Clients.All.SendAsync("UpdatePost", createdPostDTO);

            return createdPostDTO;
        }

        public async Task DeletePost(int id, int userId)
        {
            if (id < 0)
            {
                throw new ArgumentException($"Id should be positive");
            }

            var post = await _context.Posts.FirstOrDefaultAsync(p => p.Id == id);

            if (post == null)
            {
                throw new NotFoundException(nameof(Post), id);
            }
            if (post.AuthorId != userId)
            {
                throw new NotAllowedForCurrenrUserException(id);
            }

            await _context.Entry(post).Collection(p => p.Comments).LoadAsync();
            await _context.Entry(post).Collection(p => p.Reactions).LoadAsync();

            _context.Posts.Remove(post);
            await _context.SaveChangesAsync();

            await _postHub.Clients.All.SendAsync("DeletePost");
        }

        public async Task<PostDTO> GetPostById(int postId)
        {
            var post = await _context.Posts.FirstOrDefaultAsync(p => p.Id == postId);

            if (post == null)
            {
                throw new NotFoundException(nameof(Post), postId);
            }

            await _context.Entry(post).Reference(p => p.Author).LoadAsync();
            await _context.Entry(post.Author).Reference(a => a.Avatar).LoadAsync();
            await _context.Entry(post).Reference(p => p.Preview).LoadAsync();
            await _context.Entry(post).Collection(p => p.Reactions).LoadAsync();
            foreach (var reaction in post.Reactions)
            {
                await _context.Entry(reaction).Reference(p => p.User).LoadAsync();
            }
            await _context.Entry(post).Collection(p => p.Comments).LoadAsync();
            foreach (var comment in post.Comments)
            {
                await _context.Entry(comment).Collection(c => c.Reactions).LoadAsync();
                await _context.Entry(comment).Reference(c => c.Author).LoadAsync();
            }

            return _mapper.Map<PostDTO>(post);

        }
    }
}
