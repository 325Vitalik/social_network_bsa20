﻿using AutoMapper;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thread_.NET.BLL.Exceptions;
using Thread_.NET.BLL.Hubs;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Like;
using Thread_.NET.Common.Notifications;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;
using Thread_.NET.DAL.Entities.Abstract;

namespace Thread_.NET.BLL.Services
{
    public sealed class ReactionService : BaseService
    {
        private IHubContext<PostHub> _postHub;
        private EmailService _emailService;
        private EmailOptions _emailOptions;

        public ReactionService(ThreadContext context, IMapper mapper, IHubContext<PostHub> postHub, EmailService emailService, IOptions<EmailOptions> emailOptions) : base(context, mapper)
        {
            this._postHub = postHub;
            this._emailService = emailService;
            _emailOptions = emailOptions.Value;
        }

        public async Task ReactPost(NewReactionDTO reaction)
        {
            var reactions = _context.PostReactions.Where(x => x.UserId == reaction.UserId && x.PostId == reaction.EntityId);

            var result = await deleteReactionIfNecessary<PostReaction>(_context.PostReactions,
                                                                reactions,
                                                                reaction);
            if (result) return;

            await checkIfNull<Post>(_context.Posts, reaction);

            _context.PostReactions.Add(new PostReaction
            {
                PostId = reaction.EntityId,
                IsLike = reaction.IsLike,
                UserId = reaction.UserId
            });

            await _context.SaveChangesAsync();

            var postReaction = await _context.PostReactions.FirstOrDefaultAsync(p => p.PostId == reaction.EntityId && p.UserId == reaction.UserId);
            await _context.Entry(postReaction).Reference(p => p.Post).LoadAsync();
            await _context.Entry(postReaction).Reference(p => p.User).LoadAsync();
            await _context.Entry(postReaction.Post).Reference(p => p.Author).LoadAsync();

            _emailOptions.MailToAddress = postReaction.User.Email;
            _emailService.SetSettings(_emailOptions);
            await _emailService.SendNotificationAsync(postReaction.User.UserName + (postReaction.IsLike ? " liked" : " disliked") + " your post");

            await _postHub.Clients.All.SendAsync("PostReaction", reaction);
        }

        public async Task ReactComment(NewReactionDTO reaction)
        {
            var reactions = _context.CommentReactions.Where(x => x.UserId == reaction.UserId && x.CommentId == reaction.EntityId);

            var result = await deleteReactionIfNecessary<CommentReaction>(_context.CommentReactions,
                                                                            reactions,
                                                                            reaction);
            if (result) return;

            await checkIfNull<Comment>(_context.Comments, reaction);

            _context.CommentReactions.Add(new DAL.Entities.CommentReaction
            {
                CommentId = reaction.EntityId,
                IsLike = reaction.IsLike,
                UserId = reaction.UserId
            });

            await _context.SaveChangesAsync();
        }

        private async Task<bool> deleteReactionIfNecessary<TEntity>(DbSet<TEntity> entitySet, IEnumerable<TEntity> currentReactions, NewReactionDTO reaction) where TEntity : Reaction
        {
            var isNeedToRemove = currentReactions.Any(r => r.IsLike == reaction.IsLike);

            if (currentReactions.Any())
            {
                entitySet.RemoveRange(currentReactions);
                await _context.SaveChangesAsync();

                if (isNeedToRemove)
                {
                    return true;
                }
            }
            return false;
        }

        private async Task checkIfNull<TEntity>(DbSet<TEntity> entitySet, NewReactionDTO reaction) where TEntity : BaseEntity
        {
            var entity = entitySet.FirstOrDefault(ent => ent.Id == reaction.EntityId);
            if (entity == null)
            {
                throw new NotFoundException(nameof(Post), reaction.EntityId);
            }
        }
    }
}
