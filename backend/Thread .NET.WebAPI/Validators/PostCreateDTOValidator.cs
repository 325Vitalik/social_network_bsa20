﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Thread_.NET.Common.DTO.Post;

namespace Thread_.NET.Validators
{
    public sealed class PostCreateDTOValidator: AbstractValidator<PostCreateDTO>
    {
        public PostCreateDTOValidator()
        {
            RuleFor(u => u.Body).NotEmpty().When(u => string.IsNullOrEmpty(u.PreviewImage));
            RuleFor(u => u.PreviewImage).NotEmpty().When(u => string.IsNullOrEmpty(u.Body));
        }
    }
}
