﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using Thread_.NET.BLL.Services;
using Thread_.NET.Common.DTO.ResetPassword;
using Thread_.NET.Extensions;

namespace Thread_.NET.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PasswordController : ControllerBase
    {
        private readonly ResetPasswordService _resetPasswordService;

        public PasswordController(ResetPasswordService resetPasswordService)
        {
            _resetPasswordService = resetPasswordService;
        }

        [AllowAnonymous]
        [HttpPost("send-email")]
        public async Task<ActionResult> GetResetPasswordToken([FromBody] UserEmailDTO dto)
        {
            await _resetPasswordService.SendResetPasswordToken(dto.Email);
            return Ok();
        }

        [Authorize(Roles = "PasswordRecovery")]
        [HttpPost("reset")]
        public async Task<ActionResult> SendEmailWithResetPasswordLink([FromBody] ResetPasswordDTO newPassword)
        {
            var userId = this.GetUserIdFromToken();

            if (newPassword.Password != newPassword.ConfirmPassword)
            {
                throw new ArgumentException();
            }

            await _resetPasswordService.SetNewPassword(newPassword.Password, userId);

            return Ok();
        }
    }
}