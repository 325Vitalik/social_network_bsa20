﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Thread_.NET.BLL.Services;
using Thread_.NET.Common.DTO.Comment;
using Thread_.NET.Common.DTO.Like;
using Thread_.NET.Extensions;

namespace Thread_.NET.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [Authorize(Roles = "User")]
    [ApiController]
    public class CommentsController : ControllerBase
    {
        private readonly CommentService _commentService;
        private readonly ReactionService _reactionService;

        public CommentsController(CommentService commentService, ReactionService reactionService)
        {
            _commentService = commentService;
            _reactionService = reactionService;
        }

        [HttpPost]
        public async Task<ActionResult<CommentDTO>> CreateComment([FromBody] NewCommentDTO comment)
        {
            comment.AuthorId = this.GetUserIdFromToken();
            return Ok(await _commentService.CreateComment(comment));
        }

        [HttpPost("reaction")]
        public async Task<IActionResult> ReactComment(NewReactionDTO reaction)
        {
            reaction.UserId = this.GetUserIdFromToken();

            await _reactionService.ReactComment(reaction);
            return Ok();
        }

        [HttpPut("{id}")]
        public async Task<ActionResult<CommentDTO>> UpdateComment(int id, [FromBody] NewCommentDTO comment)
        {
            comment.AuthorId = this.GetUserIdFromToken();
            return Ok(await _commentService.UpdateComment(comment, id));
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteComment(int id)
        {
            var currentUserId = this.GetUserIdFromToken();
            await _commentService.DeleteComment(id, currentUserId);

            return Ok();
        }
    }
}